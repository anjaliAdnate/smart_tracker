
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';

@Injectable()
export class MenuProvider {

  constructor(public http: Http) { }

  getSideMenus() {
    return [{
      title: 'Home', component: 'DashboardPage', icon: 'home'
    }, {
      title: 'Groups', component: 'GroupsPage', icon: 'people'
    }, {
      title: 'Customers', component: 'CustomersPage', icon: 'contacts'
    }, {
      title: 'Notifications', component: 'AllNotificationsPage', icon: 'notifications'
    },
    {
      title: 'Reports',
      subPages: [{
        title: 'Daily Report',
        component: 'DailyReportPage',
      }, {
        title: 'Value Screen',
        component: 'DailyReportNewPage',
      }, {
        title: 'Speed Variation Report',
        component: 'SpeedRepoPage',
      }, {
        title: 'Summary Report',
        component: 'DeviceSummaryRepoPage',
      }, {
        title: 'Geofenceing Report',
        component: 'GeofenceReportPage',
      },
      {
        title: 'Overspeed Report',
        component: 'OverSpeedRepoPage',
      }, {
        title: 'Ignition Report',
        component: 'IgnitionReportPage',
      },
      {
        title: 'Route Violation Report',
        component: 'RouteVoilationsPage',
      }, {
        title: 'Stoppage Report',
        component: 'StoppagesRepoPage',
      },
      {
        title: 'Fuel Report',
        component: 'FuelReportPage',
      }, {
        title: 'Distnace Report',
        component: 'DistanceReportPage',
      },
      {
        title: 'Trip Report',
        component: 'TripReportPage',
      }],
      icon: 'clipboard'
    },
    {
      title: 'Routes',
      subPages: [{
        title: 'Routes',
        component: 'RoutePage',
      }, {
        title: 'Route Maping',
        component: 'RouteMapPage',
      }],
      icon: 'map'
    }, {
      title: 'Feedback', component: 'FeedbackPage', icon: 'paper'
    }, {
      title: 'Contact Us', component: 'ContactUsPage', icon: 'mail'
    }, {
      title: 'Support', component: 'SupportPage', icon: 'call'
    }, {
      title: 'About Us', component: 'AboutUsPage', icon: 'alert'
    }, ];
  }
}
